from trytond.pool import Pool, PoolMeta


class Voucher(metaclass=PoolMeta):
    __name__ = 'account.voucher'

    def _reconcile_lines(self, to_reconcile):
        super(Voucher, self)._reconcile_lines(to_reconcile)
        Loan = Pool().get('staff.loan')
        for sl, ml in to_reconcile.values():
            payments_add = {}
            origin = getattr(sl, 'move_origin', None)
            if origin and origin.__name__ == 'staff.loan':
                if origin not in payments_add:
                    payments_add[origin] = [ml]
                else:
                    payments_add[origin] += [ml]

        if payments_add:
            Loan.add_payment_lines(payments_add, self)