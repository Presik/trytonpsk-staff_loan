# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import payment_term
from . import loan
from . import configuration
from . import account
from . import voucher 


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationDefaultAccount,
        configuration.Sequence,
        loan.Loan,
        # configuration.StaffConfiguration,
        # payment_term.PaymentTermLoan,
        # payment_term.PaymentTermLoanLine,
        # payment_term.PaymentTermLoanLineRelativeDelta,
        # payment_term.TestPaymentTermView,
        payment_term.TestPaymentTermViewResult,
        account.Move,
        account.MoveLine,
        loan.LoanLine,
        loan.LoanPaymentLine,
        # loan.DayPaymentLoan,
        loan.DayPayment,
        voucher.Voucher,
        module='staff_loan', type_='model')
    Pool.register(
        loan.LoanReport,
        module='staff_loan', type_='report')
    Pool.register(
        payment_term.TestPaymentTerm,
        module='staff_loan', type_='wizard')
