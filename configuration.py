# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields, ModelSingleton
from trytond.transaction import Transaction
from trytond.pyson import Eval, Id
from trytond.pool import PoolMeta, Pool
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)


class Configuration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Loan Configuration'
    __name__ = 'loan.configuration'
    default_account_debit = fields.MultiValue(fields.Many2One(
            'account.account', "Default Account Debit",
            domain=[
                ('type', '!=', None),
                ('closed', '!=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ]))
    staff_loan_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', "Loan Sequence", required=True,
            domain=[
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None]),
                ('sequence_type', '=',
                    Id('staff_loan',
                        'sequence_type_loan')),
                ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'default_account_debit':
            return pool.get('loan.configuration.default_account')
        if field == 'staff_loan_sequence':
            return pool.get('loan.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)


class ConfigurationDefaultAccount(ModelSQL, CompanyValueMixin):
    "Loan Configuration Default Account"
    __name__ = 'loan.configuration.default_account'
    default_account_debit = fields.Many2One(
        'account.account', "Default Account Debit",
        domain=[
            ('type', '!=', None),
            ('closed', '!=', True),
            ('company', '=', Eval('company', -1)),
            ],
        depends=['company'])


class Sequence(ModelSQL, CompanyValueMixin):
    "Loan Configuration Sequence"
    __name__ = 'loan.configuration.sequence'
    staff_loan_sequence = fields.Many2One(
        'ir.sequence', "Loan Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=',
                Id('staff_loan', 'sequence_type_loan')),
            ],
        depends=['company'])
