# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.exceptions import UserError, UserWarning
from trytond.model.exceptions import ValidationError


class BadOperationError(UserError):
    pass


class PaymentTermValidationError(ValidationError):
    pass


class LoanMissingSequence(ValidationError):
    pass


class PaymentTermComputeError(UserError):
    pass
