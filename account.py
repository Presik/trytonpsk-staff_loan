from trytond.pool import PoolMeta, Pool
from trytond.i18n import gettext
from trytond.model import (fields)
from trytond.pyson import Eval, If, Bool
from trytond.tools import grouped_slice


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + ['staff.loan']


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    loan_payment = fields.Function(fields.Many2One(
            'staff.loan', "Loan Payment",
            domain=[
                ('account', '=', Eval('account', -1)),
                If(Bool(Eval('party')),
                    ('party', '=', Eval('party')),
                    (),
                    ),
                ],
            states={
                'invisible': Bool(Eval('reconciliation')),
                },
            depends=['account', 'party', 'reconciliation']),
        'get_loan_payment',
        setter='set_loan_payment',
        searcher='search_loan_payment')
    loan_payments = fields.Many2Many(
        'staff.loan-account.move.line', 'line', 'loan',
        "Loan Payments", readonly=True)

    @classmethod
    def __setup__(cls):
        super(MoveLine, cls).__setup__()
        cls._check_modify_exclude.add('loan_payment')

    @classmethod
    def _get_origin(cls):
        return super()._get_origin() + ['staff.loan.line']

    @classmethod
    def copy(cls, lines, default=None):
        default = {} if default is None else default.copy()
        default.setdefault('loan_payments', None)
        return super().copy(lines, default=default)

    @classmethod
    def get_loan_payment(cls, lines, name):
        pool = Pool()
        LoanPaymentLine = pool.get('staff.loan-account.move.line')

        ids = list(map(int, lines))
        result = dict.fromkeys(ids, None)
        for sub_ids in grouped_slice(ids):
            payment_lines = LoanPaymentLine.search([
                    ('line', 'in', list(sub_ids)),
                    ])
            result.update({p.line.id: p.loan.id for p in payment_lines})
        return result

    @classmethod
    def set_loan_payment(cls, lines, name, value):
        pool = Pool()
        Loan = pool.get('staff.loan')
        Loan.remove_payment_lines(lines)
        if value:
            Loan.add_payment_lines({Loan(value): lines})

    @classmethod
    def search_loan_payment(cls, name, domain):
        return [('loan_payments',) + tuple(domain[1:])]


def _loans_to_process(reconciliations):
    pool = Pool()
    Reconciliation = pool.get('account.move.reconciliation')
    Loan = pool.get('staff.loan')

    move_ids = set()
    others = set()
    for reconciliation in reconciliations:
        for line in reconciliation.lines:
            move_ids.add(line.move.id)
            others.update(line.reconciliations_delegated)

    loans = set()
    for sub_ids in grouped_slice(move_ids):
        loans.update(Loan.search([
                    ('account_move', 'in', list(sub_ids)),
                    ]))
    if others:
        loans.update(_loans_to_process(Reconciliation.browse(others)))

    return loans


class Reconciliation(metaclass=PoolMeta):
    __name__ = 'account.move.reconciliation'

    @classmethod
    def create(cls, vlist):
        Loan = Pool().get('staff.loan')
        reconciliations = super(Reconciliation, cls).create(vlist)
        Loan.__queue__.refresh(list(_loans_to_process(reconciliations)))
        return reconciliations

    @classmethod
    def delete(cls, reconciliations):
        Loan = Pool().get('staff.loan')

        loans_to_process = _loans_to_process(reconciliations)
        super(Reconciliation, cls).delete(reconciliations)
        Loan.__queue__.refresh(list(loans_to_process))
